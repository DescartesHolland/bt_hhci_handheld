package com.demo.bluetooth.handheld;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.Arrays;

import com.luugiathuy.apps.remotebluetooth.R;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RemoteBluetooth extends Activity {

	// Layout view
	private Button mConnectButton;
	public static String msg;
	public static boolean QR_ENABLED = true;
	private ImageView mCursorView;
	Context con;

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;

	// Message types from Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Names received from Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";
	public static final String MESSAGE = "message";

	//Commands
	private static final int CURSOR_UP = 10;
	private static final int CURSOR_RIGHT = 20;
	private static final int CURSOR_DOWN = 30;
	private static final int CURSOR_LEFT = 40;

	// Name of the connected device
	private String mConnectedDeviceName = null;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for Bluetooth Service
	private BluetoothService mCommandService = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		con = this;

		//Set message / text to send:
		msg = "Y";

		// Set up the window layout
		setContentView(R.layout.main);
		mCursorView = (ImageView) findViewById(R.id.cursor);

		//Keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		//Set up connect button
		mConnectButton = (Button) findViewById(R.id.connect_btn);
		mConnectButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(QR_ENABLED) {
					try {
						Intent intent = new Intent("com.google.zxing.client.android.SCAN");
						intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes
						startActivityForResult(intent, 0);

					} catch (Exception e) {
						mCommandService.start();
						Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
						Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
						startActivity(marketIntent);
					}
				}
				else {
					// Launch the DeviceListActivity to see devices and do scan
					Intent serverIntent = new Intent(con, DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
				}
				mConnectButton.setVisibility(View.GONE);
			}
		});

		//Set up send button
		Button sendDataButton = (Button) findViewById(R.id.send_btn);
		sendDataButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//Send String to bluetooth service to be written
				mCommandService.write(msg.getBytes());
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();

		// If BT is not on, request that it be enabled.
		// setupCommand() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
		// otherwise set up the command service
		else if (mCommandService==null)
			initializeBluetoothService();
	}
	@Override
	protected void onResume() {
		super.onResume();

		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() gets called when ACTION_REQUEST_ENABLE activity returns.
		if (mCommandService != null) {
			if (mCommandService.getState() == BluetoothService.STATE_NONE) {
				mCommandService.start();
			}
		}
	}
	private void initializeBluetoothService() {
		// Initialize the Bluetooth service to perform bluetooth connections
		mCommandService = new BluetoothService(this, mHandler);
	}
	@Override
	protected void onDestroy() {
		Log.d("RemoteBluetooth", "onDestroy()");
		super.onDestroy();
		if (mCommandService != null)
			mCommandService.stop();
	}

	/**
	 * Makes sure that the device is in discovery mode
	 */
	private void ensureDiscoverable() {
		if (mBluetoothAdapter.getScanMode() !=
				BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					break;
				case BluetoothService.STATE_CONNECTING:
					break;
				case BluetoothService.STATE_LISTEN:
					break;
				case BluetoothService.STATE_NONE:
					break;
				}
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to "
						+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
						Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_READ:
								Log.d("TEST", "Message Read");
				byte[] readBuf = (byte[]) msg.obj;
				//				Log.d("TEST", "Got readBuf: "+Arrays.toString(readBuf));
				InputStream bos = new ByteArrayInputStream(readBuf);

				//				String line = getStringFromInputStream(bos);
				//				Log.d("TEST", "Got bos; "+line);
				try {
					byte[] buffer = new byte[bos.available()];
					bos.read(buffer);
					Log.d("TEST", Arrays.toString(buffer));
					String message = new String(buffer);
										Log.d("TEST", "JJJJJJJJ "+new String(buffer));
					try {
						int command = 0;
						if(buffer.length == 1) //may be a command
							command = buffer[0];
						else 
							command = Integer.parseInt(message);
						Log.d("TEST", "Command:"+command);
						if(command == CURSOR_UP) {
							mCursorView.setY(mCursorView.getY() - 25);
						}
						else if(command == CURSOR_RIGHT) 
							mCursorView.setX(mCursorView.getX() + 25);
						else if(command == CURSOR_DOWN) 
							mCursorView.setY(mCursorView.getY() + 25);
						else if(command == CURSOR_LEFT) 
							mCursorView.setX(mCursorView.getX() - 25);
						else 
							throw new NumberFormatException();
					}
					catch(NumberFormatException e) {
						Log.d("TEST", "NumberFormatException caught; probably not a command");
						//Not a command
					}
				}
				catch (Exception e) {
					Log.d("TEST", "Exception caught");
					e.printStackTrace();
				}
				break;
			}
		}
	};

	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
				// Get the device MAC address
				String address = data.getExtras()
						.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// Get the BLuetoothDevice object
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
				// Attempt to connect to the device
				mCommandService.connect(device);
			}
			break;

		case REQUEST_ENABLE_BT:
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up a chat session
				initializeBluetoothService();
			} else {
				// User did not enable Bluetooth or an error occured
				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
				finish();
			}
		case 0:
			if(resultCode == Activity.RESULT_OK) {

				String contents = data.getStringExtra("SCAN_RESULT");
				BluetoothDevice dev = mBluetoothAdapter.getRemoteDevice(contents);
				mCommandService.connect(dev);
			}
			if(resultCode == RESULT_CANCELED){
				//handle cancel
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scan:
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;
		case R.id.discoverable:
			// Ensure this device is discoverable by others
			ensureDiscoverable();
			return true;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			return true;
		}
		else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
			return true;
		}
		else if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}